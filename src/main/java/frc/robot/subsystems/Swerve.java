package frc.robot.subsystems;

import java.io.IOException;

import org.photonvision.PhotonCamera;

import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.math.estimator.SwerveDrivePoseEstimator;
import edu.wpi.first.math.filter.SlewRateLimiter;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Swerve extends SubsystemBase {
    public SwerveDrivePoseEstimator poseEstimator;
    public SwerveModule[] mSwerveMods;
    public NavX gyro;

    private AprilTagFieldLayout fieldLayout;

    private PhotonCamera camera = new PhotonCamera("OV5647");

    private boolean braking;

    private SlewRateLimiter limiterx, limitery;

    public Swerve() {
        try {
            fieldLayout = AprilTagFieldLayout.loadFromResource(AprilTagFields.k2023ChargedUp.m_resourceFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        braking = false;
        gyro = new NavX(Port.kMXP);

        this.limiterx = new SlewRateLimiter(40);
        this.limitery = new SlewRateLimiter(40);

        mSwerveMods = new SwerveModule[] {
            new SwerveModule(0, Constants.Swerve.Mod0.constants),
            new SwerveModule(1, Constants.Swerve.Mod1.constants),
            new SwerveModule(2, Constants.Swerve.Mod2.constants),
            new SwerveModule(3, Constants.Swerve.Mod3.constants)
        };


        /* By pausing init for a second before setting module offsets, we avoid a bug with inverting motors.
         * See https://github.com/Team364/BaseFalconSwerve/issues/8 for more info.
         */
        Timer.delay(1.0);
        resetModulesToAbsolute();

        poseEstimator = new SwerveDrivePoseEstimator(Constants.Swerve.swerveKinematics, getYaw(), getModulePositions(), new Pose2d());

        zeroGyro();

    }

    public void drive(Translation2d translation, double rotation, boolean isOpenLoop) {
        drive(translation, rotation, isOpenLoop, false);
    }

    public void drive(Translation2d translation, double rotation, boolean isOpenLoop, boolean isRateLimited) {
        if(braking) return;
        SwerveModuleState[] swerveModuleStates =
            Constants.Swerve.swerveKinematics.toSwerveModuleStates(
                ChassisSpeeds.fromFieldRelativeSpeeds(
                                    isRateLimited ? limiterx.calculate(translation.getX()) : translation.getX(), 
                                    isRateLimited ? limitery.calculate(translation.getY()) : translation.getY(), 
                                    rotation, 
                                    getYaw()
                                ));
                    
        SwerveDriveKinematics.desaturateWheelSpeeds(swerveModuleStates, Constants.Swerve.maxSpeed);
        SmartDashboard.putString("desired mod0", swerveModuleStates[0].toString());
        for(SwerveModule mod : mSwerveMods){
            mod.setDesiredState(swerveModuleStates[mod.moduleNumber], isOpenLoop);
        }
    }    

    /* Used by SwerveControllerCommand in Auto */
    public void setModuleStates(SwerveModuleState[] desiredStates) {
        SwerveDriveKinematics.desaturateWheelSpeeds(desiredStates, Constants.Swerve.maxSpeed);
        SmartDashboard.putString("desired mod0", desiredStates[0].toString());     
        SmartDashboard.putString("desired mod1", desiredStates[1].toString());  
        SmartDashboard.putString("desired mod2", desiredStates[2].toString());  
        SmartDashboard.putString("desired mod3", desiredStates[3].toString());  
        for(SwerveModule mod : mSwerveMods){
            mod.setDesiredState(desiredStates[mod.moduleNumber], false);
        }
    }    

    public PhotonCamera getCamera() {
        return camera;
    }

    public Pose2d getPose() {
        return poseEstimator.getEstimatedPosition();
    }
    
    public boolean isBraking() {
        return braking;
    }

    public void resetOdometry(Pose2d pose) {
        poseEstimator.resetPosition(getYaw(), getModulePositions(), pose);
    }

    public void toggleBrake() {
        if(braking) {
            braking = false;
            setModuleStates(new SwerveModuleState[]{
                new SwerveModuleState(Constants.Swerve.maxSpeed * 0.011, Rotation2d.fromDegrees(0)),
                new SwerveModuleState(Constants.Swerve.maxSpeed * 0.011, Rotation2d.fromDegrees(0)),
                new SwerveModuleState(Constants.Swerve.maxSpeed * 0.011, Rotation2d.fromDegrees(0)),
                new SwerveModuleState(Constants.Swerve.maxSpeed * 0.011, Rotation2d.fromDegrees(0))
            });
            setModuleStates(new SwerveModuleState[]{
                new SwerveModuleState(0, Rotation2d.fromDegrees(0)),
                new SwerveModuleState(0, Rotation2d.fromDegrees(0)),
                new SwerveModuleState(0, Rotation2d.fromDegrees(0)),
                new SwerveModuleState(0, Rotation2d.fromDegrees(0))
            });
            SmartDashboard.putBoolean("Is Braking", false);
        } else {
            braking = true;
            setModuleStates(new SwerveModuleState[]{
                new SwerveModuleState(Constants.Swerve.maxSpeed * 0.011, Rotation2d.fromDegrees(-45)),
                new SwerveModuleState(Constants.Swerve.maxSpeed * 0.011, Rotation2d.fromDegrees(-135)),
                new SwerveModuleState(Constants.Swerve.maxSpeed * 0.011, Rotation2d.fromDegrees(-135)),
                new SwerveModuleState(Constants.Swerve.maxSpeed * 0.011, Rotation2d.fromDegrees(135))
            });
            setModuleStates(new SwerveModuleState[]{
                new SwerveModuleState(0, Rotation2d.fromDegrees(-45)),
                new SwerveModuleState(0, Rotation2d.fromDegrees(-135)),
                new SwerveModuleState(0, Rotation2d.fromDegrees(-135)),
                new SwerveModuleState(0, Rotation2d.fromDegrees(135))
            });
            SmartDashboard.putBoolean("Is Braking", true);
        }
    }

    public SwerveModuleState[] getModuleStates(){
        SwerveModuleState[] states = new SwerveModuleState[4];
        for(SwerveModule mod : mSwerveMods){
            states[mod.moduleNumber] = mod.getState();
        }
        return states;
    }

    public SwerveModulePosition[] getModulePositions(){
        SwerveModulePosition[] positions = new SwerveModulePosition[4];
        for(SwerveModule mod : mSwerveMods){
            positions[mod.moduleNumber] = mod.getPosition();
        }
        return positions;
    }

    public void zeroGyro(){
        gyro.zeroYaw();
        resetOdometry(new Pose2d(0, 0, new Rotation2d()));
    }

    public Rotation2d getYaw() {
        // should be ccw+
        // return Rotation2d.fromDegrees(Math.abs((Math.abs(gyro.getYaw() - 180)+180)-180));
        return Rotation2d.fromDegrees(Math.abs(gyro.getYaw()-180)-180);
    }

    public void resetModulesToAbsolute(){
        for(SwerveModule mod : mSwerveMods){
            mod.resetToAbsolute();
        }
    }

    @Override
    public void periodic(){
        poseEstimator.update(getYaw(), getModulePositions());  
        // if(camResult.hasTargets()) {
        //     PhotonTrackedTarget bestTarget = camResult.getBestTarget();
        //     if(fieldLayout.getTagPose(bestTarget.getFiducialId()).isPresent()) {
        //         Pose3d targetPose3d = fieldLayout.getTagPose(bestTarget.getFiducialId()).get();
        //         Pose2d robotPose2d = targetPose3d.transformBy(camResult.getBestTarget().getBestCameraToTarget().inverse()).toPose2d(); // centered on camera, absolute position (relative to target)
        //         SmartDashboard.putString("Estimated pose via target", robotPose2d.toString());
        //     }
        //     // pose of target
        //     // Pose3d camPose = new Pose3d(0, 0, 0, new Rotation3d()).transformBy(camResult.getBestTarget().getBestCameraToTarget().inverse());
        //     // poseEstimator.addVisionMeasurement(new Pose2d(camPose.getX(), camPose.getY(), Rotation2d.fromDegrees(camPose.getRotation().getZ())), camResult.getTimestampSeconds());
        // }
        SmartDashboard.putNumber("Roll", gyro.getRoll());
        SmartDashboard.putString("Est. Pose", poseEstimator.getEstimatedPosition().toString());
        SmartDashboard.putNumber("Heading", getYaw().getDegrees());
        SmartDashboard.putNumber("Mod 0 mps", mSwerveMods[0].getState().speedMetersPerSecond);
        for(SwerveModule mod : mSwerveMods){
            SmartDashboard.putNumber("Mod " + mod.moduleNumber + " Cancoder", mod.getCanCoder().getDegrees());
            SmartDashboard.putNumber("Mod " + mod.moduleNumber + " Integrated", mod.getPosition().angle.getDegrees());
            SmartDashboard.putNumber("Mod " + mod.moduleNumber + " Velocity", mod.getState().speedMetersPerSecond);
            // SmartDashboard.putNumber("Mod " + mod.moduleNumber + " Current", mod.getDriveMotor().getOutputCurrent());
            // SmartDashboard.putNumber("Mod" + mod.moduleNumber + " setpoint", mod.getAngleSetpoint().getDegrees());
            // SmartDashboard.putNumber("Mod" + mod.moduleNumber + " Angle", mod.getCanCoderAngle());
            // SmartDashboard.putNumber("Mod" + mod.moduleNumber + " percent output", mod.getDriveMotor().get());
        }
    }
}