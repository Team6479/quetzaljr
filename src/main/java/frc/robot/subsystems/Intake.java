// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.IntakeConstants;

public class Intake extends SubsystemBase {

  private CANSparkMax leftMotor;
  private CANSparkMax rightMotor;

  /** Creates a new Intake. */
  public Intake() {
    leftMotor = new CANSparkMax(IntakeConstants.leftMotorID, MotorType.kBrushless);
    rightMotor = new CANSparkMax(IntakeConstants.rightMotorID, MotorType.kBrushless);
    
    leftMotor.restoreFactoryDefaults();
    leftMotor.setInverted(false);
    leftMotor.setIdleMode(IdleMode.kBrake);
    leftMotor.setSmartCurrentLimit(30);
    leftMotor.enableVoltageCompensation(12);
    leftMotor.burnFlash();

    rightMotor.restoreFactoryDefaults();
    rightMotor.setInverted(true);
    rightMotor.setIdleMode(IdleMode.kBrake);
    rightMotor.setSmartCurrentLimit(30);
    rightMotor.enableVoltageCompensation(12);
    rightMotor.burnFlash();
  }

  public void setSpeed(double speed){
    leftMotor.set(speed);
    rightMotor.set(speed);
  }

  public void setVoltage(double voltage) {
    leftMotor.setVoltage(voltage);
    rightMotor.setVoltage(voltage);
  }

  @Override
  public void periodic() {
    // This method will be called onuuce per scheduler run
    // SmartDashboard.putNumber("intake velocity", leftMotor.getEncoder().getVelocity());
  }
}
