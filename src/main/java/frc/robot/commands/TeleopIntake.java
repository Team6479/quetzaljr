// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.BooleanSupplier;
import java.util.function.DoubleSupplier;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.subsystems.Intake;

public class TeleopIntake extends CommandBase {

  private Intake intake;
  private DoubleSupplier speed;
  private BooleanSupplier rightBumper;
  private boolean intaking;

  /** Creates a new TeleopIntakeCommand. */
  public TeleopIntake(Intake intake, DoubleSupplier speed, BooleanSupplier rightBumper){
    // Use addRequirements() here to declare subsystem dependencies.
    this.intake = intake;
    this.speed = speed;
    this.rightBumper = rightBumper;
    addRequirements(intake);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    intake.setSpeed(0);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute(){
    if (Robot.instance.isAutonomous()) return;
    if (speed.getAsDouble() < 0) {
      intaking = true;
    }
    else if (speed.getAsDouble() > 0 || rightBumper.getAsBoolean()) {
      intaking = false;
    }
    if (rightBumper.getAsBoolean()) {
      intake.setSpeed(1);
    }
    else if(intaking && speed.getAsDouble() == 0) {
      intake.setSpeed(-0.05);
    }
    else {
      intake.setSpeed(speed.getAsDouble());
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    intake.setSpeed(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
