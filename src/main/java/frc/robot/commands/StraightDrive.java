// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.TrapezoidProfile.Constraints;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Swerve;

public class StraightDrive extends CommandBase {
  private Swerve swerve;
  private double distance;

  private ProfiledPIDController controller;
  private double initialPosition;

  /** Creates a new StraightDrive. */
  public StraightDrive(Swerve swerve, double distance) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.swerve = swerve;
    this.distance = distance;
    this.controller = new ProfiledPIDController(50, 0, 0, new Constraints(0.8, 0.4));
    addRequirements(swerve);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    controller.setGoal(-distance);
    controller.setTolerance(0.1, 0.1);
    initialPosition = swerve.getModulePositions()[0].distanceMeters;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    SmartDashboard.putNumber("distance", initialPosition - swerve.getModulePositions()[0].distanceMeters);
    swerve.drive(new Translation2d(-controller.calculate(initialPosition - swerve.getModulePositions()[0].distanceMeters), new Rotation2d()), 0, true);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    swerve.drive(new Translation2d(0, new Rotation2d()), 0, true);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return controller.atSetpoint();
  }
}
