// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.autos;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.Swerve;
import frc.robot.commands.TrajectoryRelative;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class FromLeftBalanceAuto extends SequentialCommandGroup {
  /** Creates a new FromLeftBalanceAuto. */
  public FromLeftBalanceAuto(Swerve swerve) {
    super(
      new TrajectoryRelative("fromleft.wpilib.json", swerve),
      new BalanceAuto(swerve)
    );
  }
}
