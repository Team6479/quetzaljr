package frc.robot.autos;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.Constants.AutoConstants;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Swerve;

public class ShootAndBalanceAuto extends SequentialCommandGroup {
    // TODO: Tune with a more accutate field
    public ShootAndBalanceAuto(Swerve swerve, Intake intake) {
        super(
            new WaitCommand(0.1),
            new InstantCommand(() -> swerve.zeroGyro()),
            new InstantCommand(() -> intake.setVoltage(-8)),
            new WaitCommand(0.5),
            new InstantCommand(() -> intake.setVoltage(12)),
            new WaitCommand(1),
            new InstantCommand(() -> intake.setSpeed(0)),
            
            new InstantCommand(() -> swerve.drive(new Translation2d(-1.6 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
            new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) > 10),
            new InstantCommand(() -> swerve.drive(new Translation2d(-0.8 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
            new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) < 5),
            new InstantCommand(() -> swerve.toggleBrake()),
            new WaitCommand(0.55),
            new InstantCommand(() -> swerve.toggleBrake()),
            new InstantCommand(() -> swerve.drive(new Translation2d(0.4 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
            new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) < 7),
            new InstantCommand(() -> swerve.drive(new Translation2d(0 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
            new InstantCommand(() -> swerve.toggleBrake())
        );
    }
}
