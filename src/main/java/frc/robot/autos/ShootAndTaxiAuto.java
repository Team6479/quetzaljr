package frc.robot.autos;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.Constants.AutoConstants;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Swerve;

public class ShootAndTaxiAuto extends SequentialCommandGroup {
    // TODO: Tune with a more accutate field
    public ShootAndTaxiAuto(Swerve swerve, Intake intake) {
        super(
            new WaitCommand(0.1),
            new InstantCommand(() -> swerve.zeroGyro()),
            new InstantCommand(() -> intake.setVoltage(-8)),
            new WaitCommand(0.5),
            new InstantCommand(() -> intake.setVoltage(12)),
            new WaitCommand(1),
            new InstantCommand(() -> intake.setSpeed(0)),
            
            new InstantCommand(() -> swerve.drive(new Translation2d(-1 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
            new WaitCommand(5.5),
            new InstantCommand(() -> swerve.drive(new Translation2d(0 * AutoConstants.speedModifier, new Rotation2d()), 0, true))
            // new InstantCommand(() -> swerve.toggleBrake())
        );
    }
}
