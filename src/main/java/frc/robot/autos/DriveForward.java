// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.autos;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.Swerve;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class DriveForward extends SequentialCommandGroup {
  /** Creates a new DriveForward. */
  public DriveForward(Swerve swerve) {
    super(
      new InstantCommand(() -> swerve.zeroGyro()),
      // new InstantCommand(() -> swerve.drive(new Translation2d(1, new Rotation2d()), 0, true)),
      // new WaitCommand(5.5),
      // new InstantCommand(() -> swerve.drive(new Translation2d(0, new Rotation2d()), 0, true))
      // new InstantCommand(() -> swerve.zeroGyro()),
      new InstantCommand(() -> swerve.drive(new Translation2d(1, new Rotation2d()), 0, false)),
      new WaitCommand(1),
      new InstantCommand(() -> swerve.drive(new Translation2d(0, new Rotation2d()), 0, false))
    );
  }
}
