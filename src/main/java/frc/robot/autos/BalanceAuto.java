// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.autos;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.WaitUntilCommand;
import frc.robot.Constants.AutoConstants;
import frc.robot.subsystems.Swerve;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class BalanceAuto extends SequentialCommandGroup {
  /** Creates a new BalanceAuto. */
  public BalanceAuto(Swerve swerve) {
      super(
        new InstantCommand(() -> swerve.drive(new Translation2d(1.6 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
          new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) > 10),
          new InstantCommand(() -> swerve.drive(new Translation2d(0.8 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
          new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) < 5),
          new InstantCommand(() -> swerve.toggleBrake()),
          new WaitCommand(0.55),
          new InstantCommand(() -> swerve.toggleBrake()),
          new InstantCommand(() -> swerve.drive(new Translation2d(-0.4 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
          new WaitUntilCommand(() -> Math.abs(swerve.gyro.getRoll()) < 7),
          new InstantCommand(() -> swerve.drive(new Translation2d(0 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
          new InstantCommand(() -> swerve.toggleBrake())
      );
  }
}
