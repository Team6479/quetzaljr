package frc.robot.autos;

import java.util.List;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.TrajectoryRelative;
import frc.robot.subsystems.Swerve;

public class AroundFieldAuto extends SequentialCommandGroup {
    public AroundFieldAuto(Swerve s_swerve) {
        super(
            new InstantCommand(() -> s_swerve.zeroGyro()),
            new TrajectoryRelative(
              new Pose2d(0, 0, new Rotation2d(0)), 
              List.of(
                new Translation2d(2.5, 0)
              ), 
              new Pose2d(3.5, -2.0, new Rotation2d(0)), 
              true,
              s_swerve 
            ),
            new WaitCommand(1),
            new TrajectoryRelative(
              new Pose2d(3.5, -2.0, new Rotation2d(0)), 
              List.of(
                new Translation2d(2.5, -3.5)
              ), 
              new Pose2d(-1.0, -3.5, Rotation2d.fromDegrees(180)), 
              false,
              s_swerve 
            )
        );
    }
}
