package frc.robot.autos;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants.AutoConstants;
import frc.robot.subsystems.Swerve;

public class ThrowCubeOffAuto extends SequentialCommandGroup {
    // TODO: Tune with a more accutate field
    public ThrowCubeOffAuto(Swerve swerve) {
        super(
            new InstantCommand(() -> swerve.zeroGyro()),
            new InstantCommand(() -> swerve.drive(new Translation2d(-1.5 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
            new WaitCommand(1),
            new InstantCommand(() -> swerve.drive(new Translation2d(1.5 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
            new WaitCommand(1),
            new InstantCommand(() -> swerve.drive(new Translation2d(-1 * AutoConstants.speedModifier, new Rotation2d()), 0, true)),
            new WaitCommand(5.5),
            new InstantCommand(() -> swerve.drive(new Translation2d(0 * AutoConstants.speedModifier, new Rotation2d()), 0, true))
            // new InstantCommand(() -> swerve.toggleBrake())
        );
    }
}
