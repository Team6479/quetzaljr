package frc.robot;

import com.team6479.lib.controllers.CBXboxController;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.PowerDistribution;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.autos.DriveForward;
import frc.robot.autos.ShootAndBalanceAuto;
import frc.robot.autos.ShootAndTaxiAuto;
import frc.robot.autos.ThrowCubeAndBalanceAuto;
import frc.robot.autos.ThrowCubeOffAuto;
import frc.robot.commands.TeleopIntake;
import frc.robot.commands.TeleopSwerve;
import frc.robot.subsystems.Intake;
import frc.robot.subsystems.Swerve;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
    /* Controllers */
    private final CommandXboxController xbox = new CommandXboxController(0);

    /* Drive Controls */
    private final int translationAxis = XboxController.Axis.kLeftY.value;
    private final int strafeAxis = XboxController.Axis.kLeftX.value;
    private final int rotationAxis = XboxController.Axis.kRightX.value;

    /* Subsystems */
    private final Swerve swerve = new Swerve();
    private final Intake intake = new Intake();
    private final PowerDistribution powerDistribution = new PowerDistribution();

    private double slowSpeed = 0.5;


    // the lower limit is much lower than the upper limit because raising the arm decreases the encoder's value.
    // the PID controller is set to enable continous input, so 0 and 360 are treated as the same point
    // 306 is all the way up
    // 55 all the way down
    // moving down increases angle
    // positive speed moves arm up

    private boolean slowEnabled;

    private SendableChooser<Command> autonChooser;


    /** The container for the robot. Contains subsystems, OI devices, and commands. */
    public RobotContainer() {
        autonChooser = new SendableChooser<>();
        slowEnabled = false;

        autonChooser.setDefaultOption("shoot and balance", new ShootAndBalanceAuto(swerve, intake));
        autonChooser.addOption("shoot and taxi", new ShootAndTaxiAuto(swerve, intake));
        autonChooser.addOption("cube and balance", new ThrowCubeAndBalanceAuto(swerve));
        autonChooser.addOption("cube and taxi", new ThrowCubeOffAuto(swerve));
        autonChooser.addOption("drive forward", new DriveForward(swerve));
        autonChooser.addOption("nothing", new InstantCommand());
        Shuffleboard.getTab("Auto").add(autonChooser);

        swerve.setDefaultCommand(
            new TeleopSwerve(
                swerve, 
                () -> (-xbox.getRawAxis(translationAxis) * (slowEnabled ? slowSpeed : 1)) * 0.8,
                () -> (-xbox.getRawAxis(strafeAxis) * (slowEnabled ? slowSpeed : 1)) * 0.8, 
                () -> -xbox.getRawAxis(rotationAxis) * 0.75 * (slowEnabled ? slowSpeed : 1) * 0.6
            )
        );
        
        intake.setDefaultCommand(
            new TeleopIntake(
                intake, 
                () -> xbox.getRightTriggerAxis() - xbox.getLeftTriggerAxis(),
                xbox.rightBumper()
            )
        );


        // CameraServer.startAutomaticCapture();

        // Configure the button bindings
        configureButtonBindings();
    }

    /**
     * Use this method to define your button->command mappings. Buttons can be created by
     * instantiating a {@link GenericHID} or one of its subclasses ({@link
     * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
     * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
     */
    private void configureButtonBindings() {
        /* Driver Buttons */
        xbox.y().onTrue(new InstantCommand(() -> swerve.zeroGyro()));

        xbox.b().onTrue(new InstantCommand(swerve::toggleBrake));

        xbox.x().onTrue(new InstantCommand(() -> {
            slowEnabled = !slowEnabled;
            SmartDashboard.putBoolean("Is Slow", slowEnabled);
        }));

        xbox.leftBumper()
            .onTrue(new InstantCommand(() -> xbox.getHID().setRumble(RumbleType.kBothRumble, 1)))
            .onFalse(new InstantCommand(() -> xbox.getHID().setRumble(RumbleType.kBothRumble, 0)));

        // xbox.getButton(Button.kLeftBumper).onTrue(new InstantCommand(() -> {
        //     slowSpeed -= 0.1;
        //     SmartDashboard.putString("Slow Speed", (slowSpeed * 100) + "%");
        // }));

        // xbox.getButton(Button.kRightBumper).onTrue(new InstantCommand(() -> {
        //     slowSpeed += 0.1;
        //     SmartDashboard.putString("Slow Speed", (slowSpeed * 100) + "%");
        // }));
    }

    public void teleopPeriodic() {
        // SmartDashboard.putBoolean("pov", xbox.getPOVButton(90, true).getAsBoolean());
    }

    public void teleopInit() {
        if(swerve.isBraking()) swerve.toggleBrake();
        SmartDashboard.putBoolean("Is Slow", slowEnabled);
        SmartDashboard.putString("Slow Speed", (slowSpeed * 100) + "%");
    }

    /**
     * Use this to pass the autonomous command to the main {@link Robot} class.
     *
     * @return the command to run in autonomous
     */
    public Command getAutonomousCommand() {
        return autonChooser.getSelected();
    }
}
